import { Link } from "react-router-dom";
const robots = [
  { id: "p1", name: "OPTIMUS PRIME" },
  { id: "p2", name: "BUMBLEBEE" },
  { id: "p3", name: "WINDBLADE" },
];
const Products = () => {
  return (
    <section>
      <h1>The Products Page</h1>
      <ul>
        {robots.map((robot) => (
          <li key={robot.id}>
            <Link to={`/products/${robot.id}/${robot.name}`}>{robot.name}</Link>
          </li>
        ))}
      </ul>
    </section>
  );
};
export default Products;
