import { useParams } from "react-router-dom";

const ProductDetail = () => {
  const params = useParams();
  console.log(params.id);
  return (
    <section>
      <h1>Product Detail</h1>
      <p>id: {params.id}</p>
      <p>name: {params.name}</p>
    </section>
  );
};
export default ProductDetail;
